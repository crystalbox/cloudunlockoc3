<?php

session_start();
include_once 'common.php';
include_once 'utils.php';

if (!extension_loaded('gd') || !function_exists('gd_info')) {
    die("PHP GD library is NOT installed on your web server");
}
require_once 'captcha/captcha.php';
$_SESSION['captcha'] = captcha();

?>

<!DOCTYPE html>
<html lang="en">
    <head>

<?php
    require_once 'header.php';
?>
        <title><?php echo APP_NAME; ?>, Unlock</title>

    </head>

    <body>

<!-- Navigation -->
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?= 'index.php' ?>"><?php echo APP_NAME; ?></a>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li>
                    <a href="index.php">Home</a>
                </li>
                <li>
                    <a href="contact.php">Contact</a>
                </li>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container -->

</nav>


        <!-- Page Content -->
        <div class="container">

			<div class="row">
				<h1 class="page-header">About Cloud Unlock</h1>
				<div class="col-md-12">
					<h3>What is Cloud Unlock?</h3>
					<p>Cloud Unlock is a free online service allowing to unlock (and remove password) for Excel Spreadsheet, Word Document and PowerPoint Presentation for free.</p>
					<hr />
				</div>
			</div>
			<!-- /.row -->
		
            <div class="row">
                <h1 class="page-header">Contact</h1>
                    <div class="col-md-12">
                        <h3>Have a question?</h3>
                        <p>Feel free to ask any question using the form below:</p>
                        <hr />
                    </div>
                <div class="col-md-12">
                    <form id="contact_form" class="form-horizontal col-sm-10" role="form" method="post" action="send.php">
                        <div class="modal-body">
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="username">User Name*</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" id="username" name="username" placeholder="Your name or pseudo" required="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="email">Email*</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" id="email" name="email" placeholder="Email" required="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="nature">Nature </label>
                                <div class="col-sm-6">
                                    <select class="form-control" id="nature" name="nature">
                                        <option>Information</option>
                                        <option>Complaints</option>
                                        <option>Suggestion</option>
                                        <option>Bug</option>
                                        <option>Other</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="subject">Subject* </label>
                                <div class="col-sm-6">
                                    <textarea class="form-control" rows="7" id="subject" name="subject" required=""></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                              <label class="control-label col-sm-2" for="captcha">Captcha*:</label>
                                <div class="col-sm-8">
                                    <img src="<?= $_SESSION['captcha']['image_src'] ?>" />
                                    <input type="text" id="captcha" name="captcha" class="form-control" placeholder="Type text you see above (insensitive case)" required="" />
                                </div>
                            </div>                                
                            
                        </div><!-- modal-body -->
                        <div class="modal-footer ">
                            <button id="btn_contact" name="btn_contact" type="submit" class="btn btn-info" title="Send">Send</button>
                            <input type="hidden" id="hcontact" name="hcontact">
                        </div>	
                    </form>
                </div>

            </div>
            <!-- /.row -->
            <hr>
            
        <!-- Footer -->
        <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p>Copyright &copy; <?php echo APP_NAME.' '.date('Y'); ?></p>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </footer>

        </div>
        <!-- /.container -->

<?php
require_once 'footerjs.php';
?>
    </body>

</html>