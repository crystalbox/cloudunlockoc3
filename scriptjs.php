<script>
   
    $(function(){
        //Bootstrap-tooltip initialize
        $('[data-toggle="tooltip"]').tooltip();
        
        //Opt-in popover
        $('[data-toggle="popover"]').popover();
        
        //update disabled button 
       $('input[type="checkbox"]').click(function(){
            updateUnlockButton();
        });
        
        $('#xlsx').change(function(){
            updateUnlockButton();
        });
        
        
        $('#docx').change(function(){
            var files = $(this).get(0).files;
            $('#btnword').attr('disabled', (files.length === 0) || (files[0].name.split('.').pop().toLowerCase()!=='docx'));
        });
        
        $('#pptx').change(function(){
            var files = $(this).get(0).files;
            $('#btnslides').attr('disabled', (files.length === 0) || (files[0].name.split('.').pop().toLowerCase()!=='pptx'));
        });
        
    }); //jQuery

    function updateUnlockButton(){
        var files = $('#xlsx').get(0).files;
        var ext = files.length > 0? files[0].name.split('.').pop().toLowerCase():'';
        $('#btnexcel').attr('disabled', 
            (files.length === 0) || 
            (ext !=='xlsx' && ext !=='xlsm') ||                    
            (!$('#unlock-workbook').prop('checked') && !$('#unlock-sheets').prop('checked')) 
        );
    }
</script>