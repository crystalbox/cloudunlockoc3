<?php
session_start();
include_once 'common.php';
include_once 'utils.php';

if (!getSession($hash, 'file_hash')){
    redir('index.php');
}

?>
<!DOCTYPE html>
<html lang="en">
    <head>
<?php
require_once 'header.php';
?>
    <script src="js/jquery-latest.min.js"></script>
    <script src="js/bootstrap.min.js"></script>       
    <title><?php echo APP_NAME; ?>, Unlock</title>
</head>
<body>


<!-- Navigation -->
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?= 'index.php' ?>"><?php echo APP_NAME; ?></a>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li>
                    <a href="index.php">Home</a>
                </li>
                <li>
                    <a href="contact.php">Contact</a>
                </li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li>
                    <a href="#">
                        <span class="badge"><?php
if (file_exists(LOG_FILE)){
    $links = simplexml_load_file(LOG_FILE);
    $count = 0;
    if ($links){
        $count = count($links);
    }
    echo $count;
} else {
    echo '0';
}
?></span> Files unlocked
                    </a>
                </li>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container -->
</nav>

<div class="container">
    <div class="page-header">

        <h1>This File is Encrypted!</h1>
        <p class="lead">This file is <strong>encrypted</strong>. In order to unlock this file you need to perform a <a href="https://en.wikipedia.org/wiki/Brute-force_attack">brute force</a> against this file using a <strong>Password Recovery Tool</strong>.</p>
        <p>This page will generate a script which you can use to perform a brute-force using the most powerful <strong>Password Recovery Tool</strong>: <a href="https://hashcat.net">Hashcat</a></p>
        <p>All you need to do is download <a href="https://hashcat.net/files/hashcat-3.6.0.7z"><strong>hashcat.7z (2.5 Mb)</strong></a> (or from here: <a href="./hashcat.zip"><strong>hashcat.zip (4.34 Mb)</strong></a>) then fill the form options below, then download the script from the button at the bottom.</p>
        <p>Once you've download the script file, extract its content to any directory, and extract hashcat to the same directory then click <strong>Unlock</strong> script file.</p>

    </div><!-- .page-header -->
    
    <div class="row">
        <div class="col-sm-12">
            
        <form id="form-unlock" action="#" method="post">
            <div class="form-group">
                <div class="row">
                    <div class="col-sm-12">
                        <label for="file_hash">File Hash:</label>
                        <textarea id="file_hash" name="file_hash" class="form-control" row="4" readonly="readonly"><?= $hash ?></textarea>
                    </div>
                </div>
            </div>
            
            <fieldset>
            <legend>Brute force Options:</legend>
            <div class="form-group">
                <!--label class="control-label">Method:</label>
                <div class="row">
                    <div class="col-sm-12">
                        <label class="radio-inline"><input type="radio" id="method-wordlist" name="unlock-method" value="1" />WordList</label>
                        <label class="radio-inline"><input type="radio" id="method-dictionary" name="unlock-method" value="2" />Dictionary</label>
                        <label class="radio-inline"><input type="radio" id="method-bruteforce" name="unlock-method" value="3" checked="checked" />Brute-force</label>
                    </div>
                </div !-->
            </div>
            <div class="form-group">
                <input id="increment-mode" name="increment-mode" type="checkbox" checked="checked"/>
                <label for="increment-mode" data-toggle="tooltip" title="Specifies that the length of the password candidates shouldn't be fixed, but increase in length" >Increment Mode</label>
            </div>

            <div class="form-group">
                <div class="row">
                    <div class="col-sm-3">
                        <label class="control-label" for="hash_min">Min: </label><input class="form-control" id="hash_min" name="hash_min" value="1" data-toggle="tooltip" title="The minimum length for the increment mode" />
                    </div>
                    <div id="div-max" class="col-sm-3">
                    <label class="control-label" for="hash_max">Max: </label><input class="form-control" id="hash_max" name="hash_max" value="10" data-toggle="tooltip" title="The maximum length for the increment mode" />
                    </div>
                </div>
            </div>
            <div id="div-mask" class="form-group">
                <div class="row">
                    <div class="col-sm-6">
                        <label class="control-label" for="mask" data-toggle="tooltip" title="Mask is combination of Charset">Mask: </label>
                        <input class="form-control" type="text" id="mask" name="mask" value="?l?l?l?l?l?l?l?l?l?l" data-toggle="popover" data-content="Possible charsets:<pre>l: abcdefghijklmnopqrstuvwxyz<br />u: ABCDEFGHIJKLMNOPQRSTUVWXYZ<br />d: 0123456789<br />h: 0123456789abcdef<br />H: 0123456789ABCDEF<br />s: (32 symbols)<br />a: ?l?u?d?s<br /></pre>" title="Specifies the charsets" data-placement="right" data-html="true" />
                        <span id="msg-mask-error" class="text-danger hidden">The length of the Mask should be not longer than Max value.</span>
                    </div>                           
                </div>
            </div>
            <hr />

            <div class="form-group">
                <input id="show-status" name="show-status" type="checkbox" />
                <label for="show-status" data-toggle="tooltip" title="Check this option so hashcat showes the evolution of brute force" >Show Status</label>
            </div>
            <div class="form-group">
                <div class="row">
                    <div class="col-sm-3">
                        <label class="control-label" for="status-timer">Status Timer: </label>
                        <input class="form-control" id="status-timer" name="status-timer" value="10" disabled="true" />
                    </div>
                </div>
            </div>
            </fieldset>
            <br />
            <fieldset>
                <legend>GPU Options</legend>
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-6">
                            <label class="control-label" for="workload" data-toggle="tooltip" title="In order to give the GPU more breathing room to handle the desktop you can set a lower workload profile">Workload Profile:</label>
                            <select class="form-control" id="workload" name="workload" data-toggle="popover" data-html="true" data-content="Enable a specific workload profile:<pre>Reduced: performance profile (low latency desktop)<br />Default: performance profile<br />Tuned: performance profile (high latency desktop)<br /></pre>" title="Workload Profile">
                                <option value="1">Reduced</option>
                                <option value="2" selected="selected">Default</option>
                                <option value="3">Tuned</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-6">
                            <label class="control-label" for="gpu-temp">Abort at °C: </label>
                            <input class="form-control" type="text" id="gpu-temp" name="gpu-temp" value="80" data-toggle="tooltip" title="For security reason hashcat will abort at specific GPU temperature." />
                        </div>
                    </div>
                </div>
            </fieldset>
            <div class="form-group">
                <input type="checkbox" id="cpu-only" name="cpu-only" data-toggle="tooltip" title="Check this option if your GPU is not supported, so hashcat will use your cpu instead." />
                <label class="control-label" for="cpu-only">Use CPU Only</label>
            </div>
            <hr />
            <div class="form-group">
                <div class="row">
                    <div class="col-sm-6">
                        <input type="checkbox" id="download-hashcat" name="download-hashcat" value="false" />
                        <label class="control-label" for="download-hashcat">Download hashcat (> 4 Mb)</label>
                    </div>
                </div>
            </div>
            <br />
            <fieldset>
                <legend>Script Options:</legend>
            <div class="form-group">
                <div class="row">
                    <div class="col-sm-6">
                        <input type="checkbox" id="check_mimetype" name="check_mimetype" value="true" />
                        <label class="control-label" for="check_mimetype" data-toggle="tooltip" title="If your file is rejected because of the its type, you can bypass file check mimetype.">Check File MimeType</label>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                    <div class="col-sm-6">
                        <label class="control-label" for="osystem">Operation System:</label>
                        <select class="form-control" id="osystem" name="osystem" data-toggle="tooltip" title="If you select Windows the script will be .bat file otherwise .bin file will be generated.">
                            <option value="exe" selected="selected">Windows</option>
                            <option value="bin">Linux</option>
                        </select>
                    </div>
                </div>
                </div>
            <div class="form-group">
                <div class="row">
                    <div class="col-sm-6">
                        <label class="control-label" for="os_arc" data-toggle="tooltip" title="For 64 bit machine you need to use hashcat64 file. set 32 bit to use hashcat32 instead.">OS Architecture Type: </label>
                        <label class="radio-inline"><input type="radio" id="os_arc_64" name="os_arc" value="64" checked="checked" />64 bit</label>
                        <label class="radio-inline"><input type="radio" id="os_arc_32" name="os_arc" value="32" />32 bit</label>
                    </div>
                </div>
            </div>
            </fieldset>
                            
            <input type="hidden" id="rnd" name="rnd" value="<?= hexdec(rand(10, 99)) ?>" />
            <button class="btn btn-success" id="generate-script" name="generate-script">Generate files Script</button>
        </form>            
        
        <div class="row" id="download-link"><p></p></div>
        </div>
        
        
        <script>
    $(function(){
        var hash_min = $('#hash_min');
        var hash_max = $('#hash_max');
        var inc_mode = $('#increment-mode');
        $(inc_mode).change(function(){
            var value = !$(this).is(':checked');
            $(hash_min).attr('disabled', value);
            $(hash_max).attr('disabled', value);
        });
        var status_timer = $('#status-timer');
        $('#show-status').change(function(){
           $(status_timer).attr('disabled', !$(this).is(':checked'));
        });
        $('input[name=unlock-method]').change(function(){
            var value = $(this).val() != 3;
            $('#mask').attr('disabled', value);
            $(inc_mode).attr('disabled', value);
            $(hash_min).attr('disabled', value);
            $(hash_max).attr('disabled', value);
        });
        
        $('#generate-script').click(function(e){
            var formData = $('#form-unlock').serialize();
            $.post('generate.php', formData, function(data){
                $('#download-link').html('<div class="col-sm-12">'+
                    '<div class="alert alert-success center" >'+
                '<p class="text-center"><a target="_blank" href="'+
                    data+'">Download script file (type: zip, ads: Adfly)</a></p></div></div>');
            });
            e.preventDefault();
            $(this).hide();
        });
        
        
    }); //$({})
        </script>        
        
    </div><!-- .row -->
    
    <!-- Footer -->
     <footer>
         <div class="row">
             <div class="col-lg-12">
                 <p>Copyright &copy; <?php echo APP_NAME.' '.date('Y'); ?></p>
             </div><!-- /.col-lg-12 -->
         </div><!-- /.row -->
     </footer>
       
</div><!-- .container -->
        
</body>

</html>