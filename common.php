<?php

include_once './adfly/example.php';

date_default_timezone_set('UTC');
define('EMAIL_CONTACT', 'admin@cloudunlock.net');
define('APP_NAME', 'Cloud Unlock');
define('CREATOR', 'CloudUnlock');
define('DEBUG_MODE', false);// $_SERVER['REMOTE_ADDR']=='::1');
define('MAX_FILE_SIZE', 2);
define('UPLOAD_DIR', 'uploads/');
define('LOG_FILE', './log/log_'.date('Y').'.xml');

/**
 * Adfly Url Shortener PHP API
 * 
 * @param string $url http://www.google.com
 * @param string $key 7abccd03cc3005835cc61dd956b583ca
 * @param int $uid 1234
 * 
 * @param string $advert_type (optional) int || banner
 * @param string $domain (optional) adf.ly || q.gs
 */
/*function adfly($url, $key, $uid, $domain = 'adf.ly', $advert_type = 'int')
{
  // base api url
  $api = 'http://api.adf.ly/api.php?';

  // api queries
  $query = array(
    'key' => $key,
    'uid' => $uid,
    'advert_type' => $advert_type,
    'domain' => $domain,
    'url' => $url
  );

  // full api url with query string
  $api = $api . http_build_query($query);
  // get data
  if ($data = file_get_contents($api))
    return $data;
}*/

/*

//Usage:

// Your api key
$apiKey = '7abccd03cc3005835dc61dd956b583ca';
// Your user id
$uId = 111111;

echo adfly('http://w3bees.com', $apiKey, $uId);

//Output:

http://adf.ly/Wtvpt
*/


function shortener($url) {
    $ex = new PhpExample();
    $res = $ex->shorten(array($url), 'adfly.local');
    if ($res['data']){
        $shortenedUrl1 = $res['data'][0];
        $hash1 = substr($shortenedUrl1['short_url'],strrpos($shortenedUrl1['short_url'],'/')+1);
        return $hash1;
    }
    return '';
}


function getUrl(){
    return ((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off') 
        || $_SERVER['SERVER_PORT'] == 443?'https://':'http://').$_SERVER['HTTP_HOST'];
}

//http://stackoverflow.com/questions/12553160/getting-visitors-country-from-their-ip
function ip_info($ip = NULL, $purpose = "location", $deep_detect = TRUE) {
    $output = NULL;
    if (filter_var($ip, FILTER_VALIDATE_IP) === FALSE) {
        $ip = $_SERVER["REMOTE_ADDR"];
        if ($deep_detect) {
            if (filter_var(@$_SERVER['HTTP_X_FORWARDED_FOR'], FILTER_VALIDATE_IP))
                $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
            if (filter_var(@$_SERVER['HTTP_CLIENT_IP'], FILTER_VALIDATE_IP))
                $ip = $_SERVER['HTTP_CLIENT_IP'];
        }
    }
    $purpose    = str_replace(array("name", "\n", "\t", " ", "-", "_"), NULL, strtolower(trim($purpose)));
    $support    = array("country", "countrycode", "state", "region", "city", "location", "address");
    $continents = array(
        "AF" => "Africa",
        "AN" => "Antarctica",
        "AS" => "Asia",
        "EU" => "Europe",
        "OC" => "Australia (Oceania)",
        "NA" => "North America",
        "SA" => "South America"
    );
    if (filter_var($ip, FILTER_VALIDATE_IP) && in_array($purpose, $support)) {
        $ipdat = @json_decode(file_get_contents("http://www.geoplugin.net/json.gp?ip=" . $ip));
        if (@strlen(trim($ipdat->geoplugin_countryCode)) == 2) {
            switch ($purpose) {
                case "location":
                    $output = array(
                        "city"           => @$ipdat->geoplugin_city,
                        "state"          => @$ipdat->geoplugin_regionName,
                        "country"        => @$ipdat->geoplugin_countryName,
                        "country_code"   => @$ipdat->geoplugin_countryCode,
                        "continent"      => @$continents[strtoupper($ipdat->geoplugin_continentCode)],
                        "continent_code" => @$ipdat->geoplugin_continentCode
                    );
                    break;
                case "address":
                    $address = array($ipdat->geoplugin_countryName);
                    if (@strlen($ipdat->geoplugin_regionName) >= 1)
                        $address[] = $ipdat->geoplugin_regionName;
                    if (@strlen($ipdat->geoplugin_city) >= 1)
                        $address[] = $ipdat->geoplugin_city;
                    $output = implode(", ", array_reverse($address));
                    break;
                case "city":
                    $output = @$ipdat->geoplugin_city;
                    break;
                case "state":
                    $output = @$ipdat->geoplugin_regionName;
                    break;
                case "region":
                    $output = @$ipdat->geoplugin_regionName;
                    break;
                case "country":
                    $output = @$ipdat->geoplugin_countryName;
                    break;
                case "countrycode":
                    $output = @$ipdat->geoplugin_countryCode;
                    break;
            }
        }
    }
    return $output;
}
/*
function getRealIpAddr(){
    if (!empty($_SERVER['HTTP_CLIENT_IP']))   //check ip from share internet
    {
      $ip=$_SERVER['HTTP_CLIENT_IP'];
    }
    elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))   //to check ip is pass from proxy
    {
      $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
    }
    else
    {
      $ip=$_SERVER['REMOTE_ADDR'];
    }
    return $ip;
}*/


/* creates a compressed zip file 
function create_zip($files = array(),$destination = '',$overwrite = false) {
	//if the zip file already exists and overwrite is false, return false
	if(file_exists($destination) && !$overwrite) { return false; }
	//vars
	$valid_files = array();
	//if files were passed in...
	if(is_array($files)) {
            //cycle through each file
            foreach($files as $file) {
                //make sure the file exists
                if(file_exists($file)) {
                    $valid_files[] = $file;
                }
            }
	}
	//if we have good files...
	if(count($valid_files)) {
		//create the archive
		$zip = new ZipArchive();
		if($zip->open($destination,$overwrite ? ZIPARCHIVE::OVERWRITE : ZIPARCHIVE::CREATE) !== true) {
			return false;
		}
		//add the files
		foreach($valid_files as $file) {
			$zip->addFile($file,$file);
		}
		//debug
		//echo 'The zip archive contains ',$zip->numFiles,' files with a status of ',$zip->status;
		
		//close the zip -- done!
		$zip->close();
		
		//check to make sure the file exists
		return file_exists($destination);
	}
	else
	{
		return false;
	}
}*/

function log_to_file($info){
    $data = file_exists(LOG_FILE)? simplexml_load_file(LOG_FILE):
        simplexml_load_string("<?xml version=\"1.0\"?>\n<links>\n</links>");
    $link = $data->addChild('link');
    foreach ($info as $tag => $value) {
        $link->addChild($tag, $value);
    }
    saveToXML(LOG_FILE, $data->asXML());
}

function isSmtpRunning(){
    $result = false;
    $f = @fsockopen('smtp host', 25) ;
    if ($f !== false) {
        $res = fread($f, 1024) ;
        $result = (strlen($res) > 0 && strpos($res, '220') === 0);
    }
    @fclose($f) ;
    return $result;
}


function unzip($file, $path){
    // assuming file.zip is in the same directory as the executing script.
    // $path = pathinfo(realpath($dir_name), PATHINFO_DIRNAME);
    $zip = new ZipArchive;
    $res = $zip->open($file);
    if ($res === TRUE) {
      // extract it to the path we determined above
      $zip->extractTo($path);
      $zip->close();
      return true;
    } else {
      return false;
    }
}

function zipAll($rootPath, $fileZip){
    // Initialize archive object
    $zip = new ZipArchive();
    $zip->open($fileZip, ZipArchive::CREATE | ZipArchive::OVERWRITE);
    // Create recursive directory iterator
    /** @var SplFileInfo[] $files */
    $files = new RecursiveIteratorIterator(
        new RecursiveDirectoryIterator($rootPath),
        RecursiveIteratorIterator::LEAVES_ONLY
    );
    foreach ($files as $name => $file)
    {
        // Skip directories (they would be added automatically)
        if (!$file->isDir())
        {
            // Get real and relative path for current file
            $filePath = $file->getRealPath();
            $relativePath = substr($filePath, strlen($rootPath) + 1);
            // Add current file to archive
            $zip->addFile($filePath, $relativePath);
        }
    }
    // Zip archive will be created only after closing object
    $zip->close();
}

function zip_file($file_name, $overwrite = false){
    $archive_file = replace_extension($file_name, 'zip');
    $zip = new ZipArchive();
    if ($zip->open($archive_file, $overwrite ? ZIPARCHIVE::OVERWRITE : ZIPARCHIVE::CREATE) != TRUE) {
        return false;
    }
    $zip->addFile($file_name, basename($file_name));
    $zip->close(); 
    return file_exists($archive_file);
}

function delTree($dir) {
   $files = array_diff(scandir($dir), array('.','..')); 
    foreach ($files as $file) { 
      (is_dir("$dir/$file")) ? delTree("$dir/$file") : unlink("$dir/$file"); 
    }
    rmdir($dir); 
}

function delFiles($dir, $filter = "*.*"){
    foreach (glob($dir.$filter) as $filename) {
        if (is_file($filename)) {
            unlink($filename);
        }
    }        
}

/*
 * calculate the hash of encrypted file
 * if you want to solve the hash online you need a super computer
 * you may try this out: https://www.nimbix.net/nimbix-cloud-demand-pricing/
 * $file_type: office|pdf
 */
function get_hash_file($file_path, $file_type = 'office') {
    $output = "";
    exec("python libs/".$file_type."2john.py " . $file_path, $output);
    $hash = implode('', $output);
    $h = (string) $hash;
    if ($file_type == 'pdf'){
        $result = '';
        if (!empty($h)){
            $result = substr($h, strpos($h, ':')+1);
            $result = substr($result, 0, strpos($result, ':'));
        }
        return $result;
    }
    return $h;
}

function replace_extension($filename, $new_extension) {
    $info = pathinfo($filename);
    return $info['dirname'].DIRECTORY_SEPARATOR.$info['filename'].'.'.$new_extension;
}

?>
