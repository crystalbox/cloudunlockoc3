<?php
session_start();
include_once 'common.php';
include_once 'utils.php';
?>

<!DOCTYPE html>
<html lang="en">
    <head>


<?php
require_once 'header.php';
?>
        <title><?php echo APP_NAME; ?>, Message Sent</title>

    </head>

    <body>
        

<!-- Navigation -->
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?= 'index.php' ?>"><?php echo APP_NAME; ?></a>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li>
                    <a href="index.php">Home</a>
                </li>
                <li>
                    <a href="contact.php">Contact</a>
                </li>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container -->

</nav>

        <!-- Page Content -->
        <div class="container">

            <div class="row">
		<h1 class="page-header">Your Message :</h1>
		<div class="col-md-12">
                                    
<?php

if ( 
	(isPost('btn_contact')) &&
	(isPost('username')) &&
	(isPost('email')) &&
	(isPost('subject')) &&
        (isPost('captcha'))
	){
    
        if (strtoupper(getPostValue('captcha')) != strtoupper($_SESSION['captcha']['code'])){
            echo '<div style="color:red;">Cannot be send, reason: <p>wrong captcha.</p></div><div class="col-sm-12"><p>Please back to <a href="contact.php">Contact</a> page and try again.</p></div>';
            return;
        }
		$nom = getPostValue('username');
		$email = getPostValue('email');
		$message = getPostValue('subject');

if (mail('bitsnaps@yahoo.fr', 'CloudUnlock, from: '.$nom, $message, 
	'From: '.$nom.' <'.$email.'>'."\n".'MIME-Version: 1.0'."\n".'Content-type: text/html; utf-8'."\r\n")) {
		echo '<div style="color:green;"><p>Your message has been sent, we will reply to you as soon as possible.</p>';
		echo '</p>Thank you for using our service.</p></div>';
	} else {
		echo '<div style="color:red;"><p>Could not be send, </p></div><div><p>Please back to <a href="contact.php">Contact</a> page and try again.</p></div>';
	}
} else {
	echo '<div style="color:red;"><p>Cloud not be send, reason: An error has occured. </p><p>Please back to <a href="contact.php">Contact</a> page and try again.</p></div>';
}

?>
                    <div>
                        <p>Back to <a href="index.php">Home</a></p>
                    </div><!-- .col-md-12 -->
                                    
                </div><!-- .row -->
                   
            <hr>
            
        <!-- Footer -->
        <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p>Copyright &copy; <?php echo APP_NAME.' '.date('Y'); ?></p>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </footer>
        
            </div><!-- container -->
    </div>

<?php
require_once 'footerjs.php';
?>
        
</body>

</html>