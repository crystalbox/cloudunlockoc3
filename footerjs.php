
<?php
if (DEBUG_MODE){
?>
<script src="<?= '/'.basename(__DIR__) ?>/js/jquery-latest.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="<?= '/'.basename(__DIR__) ?>/js/bootstrap.min.js"></script>

<?php
} else {
?>
<script src="https://code.jquery.com/jquery-latest.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>

<?php } ?>