<?php

session_start();
include_once 'common.php';
include_once 'utils.php';
define('INPUT_FILE_UPLOAD', 'file_upload');
$supported_extensions = array('xls', 'xlsx', 'xlsm', 'doc', 'docx', 'ppt', 'pptx', 'pdf');
$encrypted_mimetype = array('application/CDFV2-encrypted', 'application/CDFV2-corrupt');


if (isSetPost('rnd')){
    $command = '';
    getPost($hash, 'file_hash');
    
    $unlock_method = 3;
    getPost($unlock_method, 'unlock-method');
    $command .= "-a $unlock_method ";
    
    if (getPost($workload, 'workload')){
        $command .= "-w $workload ";
    }
    
    //gotten from https://hashcat.net/wiki/doku.php?id=example_hashes
    $version = get_file_version($hash, 'office');
    $command .= "-m $version ";
    
    if (getPost($gpu_temp, 'gpu-temp')){
        $command .= " --gpu-temp-abort=$gpu_temp";
    }
    
    if (getPost($cpu_only, 'cpu-only')){
        $command .= ' -D 1';
    }

    if (getPost($show_status, 'show-status')){
        $command .= ' --status';
        if (getPost($status_timer, 'status-timer')){
            $command .= " --status-timer=$status_timer";
        }
    }
    
    $command .= ' '.$hash;
    
    //if Brute-force mode
    if ($unlock_method == 3){
        if (getPost($increment_mode, 'increment-mode')){
            if (getPost($hash_min, 'hash_min') &&
                getPost($hash_max, 'hash_max')){
                    $command .= " -i --increment-min=$hash_min --increment-max=$hash_max ";
                }
        }
        if (getPost($mask, 'mask')){
            $command .= " $mask";
        }
    }
    
    $ext = '.bat';
    $arch = '64';
    if (getPost($os_arc, 'os_arc')){
        $arch = $os_arc;
    }
    $file_script = '';
    if (getPost($osystem , 'osystem')){
        if ($osystem == 'exe'){
            $file_script = 'unlock.bat';
        } else {
            $file_script = 'unlock.sh';
        }
    }
    make_script_file($file_script, "hashcat$arch.$osystem $command");
/*    //create script files
    $generated_files = array(
        'unlock.bat' => 'hashcat64.exe',
        'unlock32.bat' => 'hashcat32.exe',
        'unlock.sh' => 'hashcat64.bin',
        'unlock32.sh' => 'hashcat32.bin'
        );
    foreach ($generated_files as $file_script => $file_command) {
        make_script_file($file_script, "$file_command $command");
    }
*/    
    if (getPost($download_hashcat, 'download-hashcat')){
        unzip('hashcat.zip', './unlocker');
    } else {
        if (!file_exists('unlocker')) {
            mkdir('unlocker', 0777, true);
        }
    }
    
//    foreach ($generated_files as $file_script => $file_command) {
        rename($file_script, './unlocker/'.$file_script);
//    }
    $file_zip = './uploads/'.time().'_unlocker.zip';
    zipAll('./unlocker', $file_zip);
    delTree('./unlocker');
    
    $down_file = '';
    if (DEBUG_MODE){
        $down_file = $file_zip;
    } else {
        $h = shortener(getUrl().DIRECTORY_SEPARATOR.$file_zip);
        $down_file = $h !== ''? ('http://adf.ly/'.$h) : $file_zip;
    }

    $data = array(
            'file' => 'office',
            'url' => $down_file,
            'date' => date('d-m-Y'),
            'country'=> ip_info("Visitor", "Country"),
            'state' => ip_info("Visitor", "State")
            //'city' => ip_info("Visitor", "City")
            );
    log_to_file($data);
    
    echo $down_file;
    
// if no rnd found
} else {
    
if (empty($_FILES[INPUT_FILE_UPLOAD])){
    echo json_encode(['error' => 'No files found for upload.']);
    return;
}

/*
// it works for single file upload (or uploadAsync: true)
if ($_FILES[INPUT_FILE_UPLOAD]['error'] !== 0){
    echo json_encode(['error' => 'Error while uploading files.']);
    return;
}*/

//get the uploaded file
$files = $_FILES[INPUT_FILE_UPLOAD];

// get list of files name
$filenames = $files['name'];

//process uploaded files
$errors = [];
$output = [];
for ($i = 0; $i < count($filenames); $i++) {
    //$output .= $files['name'][$i].'('.filesize_formatted($files['tmp_name'][$i]).'), ';
    $filename = $filenames[$i];
    $ext = strtolower(pathinfo(basename($filename), PATHINFO_EXTENSION));
    //check file extension
    if (!in_array($ext, $supported_extensions)){
        echo json_encode(['error' => "File extension ($ext) is not supported."]);
        return;
    }
    
    $filesize = $files['size'][$i];
    if ($filesize > (MAX_FILE_SIZE * 1024*1024 /* = in Mb */) ) {
        echo json_encode(['error' => "Sorry, maximum file ($filename) size limit has been exceeded!"]);
        return;
    }
    
    if (getPost($check_mimetype, 'check_mimetype') && ($check_mimetype == 'true')){
        //check for encrypted files using FileMimeType
        $tmp_name = $files['tmp_name'][$i];
        $finfo = new finfo();
        $fileMimeType = $finfo->file($tmp_name, FILEINFO_MIME_TYPE);
        if (!in_array($fileMimeType, $encrypted_mimetype)){
            array_push($errors, '<strong>"'.$filenames[$i].'"</strong>' );
        }
    }
    //extract small extension abbreviation (ex: docx -> doc, xlsm -> xls...)
    $ext_small = substr($ext, 0, 3);
    $hash = get_hash_file($files['tmp_name'][$i], ($ext == 'pdf'?'pdf':'office') );
    $is_valid = !empty($hash);
    
    //building command
    $cmd = '';
    $unlock_method = 3;
//    if (getPost($unlock_method, 'unlock-method')){ //not yet implemented
        $cmd .= "-a $unlock_method ";
//    }
    
    if (getPost($workload, 'workload')){
        $cmd .= "-w $workload";
    }
    
    $version = get_file_version($hash, $ext);
    if (!empty($version)){
        $cmd .= " -m $version";        
    }
    
    if (getPost($gpu_temp, 'gpu_temp')){
        $cmd .= " --gpu-temp-abort=$gpu_temp";        
    }
    if (getPost($cpu_only, 'cpu_only')){
        $cmd .= " -D 1";        
    }
            
    if (getPost($show_status, 'show_status')){
        $cmd .= ' --status';
        if (getPost($status_timer, 'status_timer')){
            $cmd .= " --status-timer=$status_timer";
        }
    }
    $cmd .= ' '.$hash;
    
    //if Brute-force mode
//    if ($unlock_method == 3){ //not yet implemented
        if (getPost($inc_mode, 'inc_mode')){
            if (getPost($hash_min, 'hash_min') && getPost($hash_max, 'hash_max')){
                    $cmd .= " -i --increment-min=$hash_min --increment-max=$hash_max ";
                }
        }
        if (getPost($mask, 'mask')){
            $cmd .= " $mask";
        }
//    }

    $os_arc = "exe";
    getPost($os_arc, 'os_arc');
    $osystem = "64";
    getPost($osystem , 'osystem');

    $file_script = 'unlock.bat';
    make_script_file($file_script, "hashcat$os_arc.$osystem $cmd");
    
    if (!file_exists('unlocker')) {
        mkdir('unlocker', 0777, true);
    }
    $dir_name = '.'.DIRECTORY_SEPARATOR.'unlocker'.DIRECTORY_SEPARATOR;
    rename($file_script, $dir_name.$file_script);
    $down_file = $dir_name.$file_script;
    $zip_file = /*md5(uniqid())*/time(). '_unlock_'.basename(replace_extension($filename, 'zip'));
    zipAll($dir_name, $zip_file);
    
    array_push($output,
            '<img src="img/'.$ext_small.'_icon.png" class="file-preview-image'.($is_valid?'':' grayscale').'" alt="'
            .strtoupper($ext_small).' File" title="'.strtoupper($ext_small).' File" />'
            .'<p>'.($is_valid?$filename:"<s>$filename</s>").'</p>'
            .'<p>'.($is_valid?'<a class="btn btn-success" href="'.$zip_file.'">Unlock Script</a>':
                '<a href="#" class="btn btn-danger" disabled="disabled">Cannot be unlocked</a>').'</p>');
    
    $data = array(
            'file' => $ext,
            'url' => $down_file,
            'date' => date('d-m-Y'),
            'country'=> ip_info("Visitor", "Country"),
            'state' => ip_info("Visitor", "State")
            //'city' => ip_info("Visitor", "City")
            );
    log_to_file($data);
    
} //for Loop

if (!empty($errors)){
    echo json_encode(['error' => 'File(s) '.implode(', ', $errors).' is/are not encrypted, please remove it/them and try again."']);
    return;
}

if (!empty($output)){
    echo json_encode([
        'initialPreview' => $output/*,
        'initialPreviewConfig' => [
            'caption' => 'Word File',
            'width' => '120px',
            'url' => 'http://localhost:81/unlock',
            'key' => '100'            
        ]*/
            ]);
    return;
} else {
    echo json_encode(['error' => 'Sorry, nothing to unlock.']);
    return;
}

// check the nbr of files
//echo json_encode(['error' => 'you uploded: '.count($files['name']).' files.']);

//$path = [$files['tmp_name']];
return;

//$output = system('cd');
/*/echo $output;
if (count($_FILES[INPUT_FILE_UPLOAD]) > 1){
    echo json_encode(['error' => 'Veuillez charger un seul fichier.']);
} else {
    echo json_encode(['initialPreview' =>  
//           "<img src='img/excel_locked.png' class='file-preview-image' alt='Excel Locked' title='Excel Locked'>"
            '<p>File Uploaded.</p>'
        ]);
}
*/
}

function make_script_file($file_name, $script){
    $file = fopen($file_name, 'w');
    fwrite($file, "cd hashcat\n");
    fwrite($file, $script);
    fclose($file);    
}

function get_file_version($hash, $ext){
    $ver = '';
    if ($ext == 'pdf'){
        if (strpos($hash, '$pdf$1*2') !== false) {
            //PDF 1.1 - 1.3 (Acrobat 2 - 4)
            $ver = "10400";
        } else if (strpos($hash, '$pdf$2*3') !== false) {
            //PDF 1.4 - 1.6 (Acrobat 5 - 8)
            $ver = "10500";
        } else if (strpos($hash, '$pdf$4*4') !== false) {
            //seems to be same as the above one (1.4 - 1.6)
            $ver = "10500";
        } else if (strpos($hash, '$pdf$5*5') !== false) {            
            //PDF 1.7 Level 3 (Acrobat 9)
            $ver = "10600";
        } else if (strpos($hash, '$pdf$5*6') !== false) {            
            //PDF 1.7 Level 8 (Acrobat 10 - 11)
            $ver = "10700";
        } else {
            $ver = '';
        }
    } else {
        if (strpos($hash, '$oldoffice$1*') !== false) {
            $ver = "9700";
        } else if (strpos($hash, '$oldoffice$3*') !== false) {
            $ver = "9800";        
        } else if (strpos($hash, '$office$*2007*') !== false) {
            $ver = "9400";
        } else if (strpos($hash, '$office$*2010*') !== false) {
            $ver = "9500";
        } else if (strpos($hash, '$office$*2013*') !== false) {
            $ver = "9600";
        } else {
            $ver = '';
        }
    }
    return $ver;
}


?>