<?php

session_start();
include_once 'common.php';
include_once 'utils.php';

?>
<!DOCTYPE html>
<html lang="en">
<head>    
<?php
    require_once 'header.php';
?>
    <style>
        form label, form input {
            cursor: pointer;
        }
    </style>
    <title><?php echo APP_NAME; ?> your files</title>
</head>

<body>

<!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?= 'index.php' ?>"><?php echo APP_NAME; ?></a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li>
                        <a href="contact.php">Contact</a>
                    </li>
                    <li>
                        <a href="password_recovery.php">Password Recovery Tool</a>
                    </li>
                    <!--li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button">Other Tools<span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="password_recovery.php">Password Recovery Tool</a></li>
                        </ul>                    
                    </li !-->
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a href="#">
                            <span class="badge">
                            <?php
if (file_exists(LOG_FILE)){
    $links = simplexml_load_file(LOG_FILE);
    $count = 0;
    if ($links){
        $count = count($links);
    }
    echo $count;
} else {
    echo '0';
}
?></span> Files unlocked</a>
                    </li>
                </ul>
                              
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
            
    </nav>
	
    <div id="search-resultbox" style="display: none;" class="modal-content">
        <ul id="search-results" class="list-unstyled">
        </ul>
    </div>


    <!-- Page Content -->
    <div class="container">

        <div class="row">

            <!-- Main Content -->
            <div class="col-sm-12">
                <div class="row">
                    <div class="col-sm-12">
                        <h3>Unlock your Document (Excel & Word & PowerPoint) for Free</h3>
                        <p>Whether you forget a password for modification authorization or just you want to modify a document for some legal reason, and whether it's excel spreadsheet, word document or powerpoint presentation, you're in the right place!</p>
                        <p>This cloud application is a free service which allows you to remove password from your documents in order to have access to modify them without need any technical knowloedges neither to spending time looking for solution, just upload your document and hit Unlock!.</p>
                        <div class="alert alert-success alert-dismissible" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <ul class="list-unstyled">
                                <li><span class="glyphicon glyphicon-ok"></span> Nothing to Download or Install</li>
                                <li><span class="glyphicon glyphicon-ok"></span> No Credit Card Required & 100% Free </li>
                                <li><span class="glyphicon glyphicon-ok"></span> No Waiting Time & Simple to Use</li>
                            </ul>
                        </div>
                    </div><!-- .col-sm-12 -->
                </div><!-- .row -->
                <form action="unlock.php" method="post" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <h2>Protected Excel Workbook</h2>
                                            <img src="img/excel_workbook2.png" class="img-responsive" width="80%" />
                                        </div>
                                        <div class="col-sm-6">
                                            <h2>Protected Excel Worksheets</h2>
                                            <img src="img/excel_worksheets.jpg" class="img-responsive"  width="80%" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <legend>1. Upload Excel (XLSX or XLSM)</legend>
                                        <label for="xlsx" class="control-label">Select SpreadSheet File: </label>
                                        <input class="form-control" id="xlsx" name="xlsx" type="file" data-toggle="tooltip" title="Excel Document *.XLSX or *.XLSM Only (<?=MAX_FILE_SIZE ?>Mb MAX)." />
                                    </div>
                                </div><!-- .col -->
                                <div class="col-sm-12">
                                        <legend>2. Select Options:</legend>
                                    <div class="form-group">
                                        <label class="control-label"><input id="unlock-workbook" name="unlock-workbook" type="checkbox" checked="checked" /> Unlock Workbook</label>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label"><input id="unlock-sheets" name="unlock-sheets" type="checkbox" checked="checked" /> Unlock All WorkSheets</label>
                                    </div>
                                </div><!-- .col -->
                                <div class="col-sm-12">
                                    <input class="btn btn-success" type="submit" id="btnexcel" name="btnexcel" value="Unlock!" disabled="true" />
                                    <input type="hidden" name="hfile" id="hfile" />
                                </div><!-- .col -->
                            </div><!-- .row -->
                            
                        </div><!-- .col -->
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="row">
                                        <div class="col-sm-6 col-sm-offset-3">
                                            <h2>Protected Word Document</h2>
                                            <img src="img/protect_document_2007.png" class="img-responsive" width="100%" />
                                        </div>
                                    </div><!-- .row -->
                                    <div class="form-group">
                                        <legend>1. Upload Word Document (DOCX)</legend>
                                        <label for="docx" class="control-label">Select Word Document File: </label>
                                        <input class="form-control" id="docx" name="docx" type="file" data-toggle="tooltip" title="Word Document *.DOCX Only (<?=MAX_FILE_SIZE ?>Mb MAX)." />
                                    </div>
                                <div class="form-group">
                                    <input class="btn btn-success" type="submit" id="btnword" name="btnword" disabled="true" value="Unlock!" />
                                </div><!-- .col -->
                                    
                                    
                                </div><!-- .col-sm-12 -->
                            </div><!-- .row -->
                            
                                
                        </div><!-- .col-md-6 -->

                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <h2>Protected Presentation</h2>
                                            <img src="img/powerpoint_locked.png" class="img-responsive" width="60%" />
                                        </div>
                                    </div><!-- .row -->
                                    <div class="form-group">
                                        <legend>1. Upload Presentation Document (PPTX)</legend>
                                        <label for="pptx" class="control-label">Select Presentation Document File: </label>
                                        <input class="form-control" id="pptx" name="pptx" type="file" data-toggle="tooltip" title="Presentation Document *.PPTX Only (<?=MAX_FILE_SIZE ?>Mb MAX)." />
                                    </div>
                                <div class="form-group">
                                    <input class="btn btn-success" type="submit" id="btnslides" name="btnslides" disabled="true" value="Unlock!" />
                                </div><!-- .col -->
                                    
                                </div><!-- .col-sm-12 -->
                            </div><!-- .row -->
                            
                                
                        </div><!-- .col-md-12 -->
                        
                    </div><!-- .row -->
		</form>

            </div><!-- .col-sm-12 -->
            <div class="col-sm-12">
                <br />
                <p><strong>Notes</strong>: </p>
                <ul class="list-circle">
                    <li>We cannot decrypt encrypted documents with a password.</li>
                    <li>We do not keep a copy of your file in our server, so please keep a copy of your original file.</li>
                    <li>A small AdFly ads will be shown once download link generated, this is to keep our service free for everyone.</li>
                </ul>
            </div>

        </div><!-- /.row -->
        
        <hr />

        <!-- Footer -->
        <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p>Copyright &copy; <?php echo APP_NAME.' '.date('Y'); ?></p>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </footer>

    </div>
    <!-- /.container -->

<?php
    require_once 'footerjs.php';
?>
    

<?php
    include_once 'scriptjs.php';
?>


        
</body>

</html>