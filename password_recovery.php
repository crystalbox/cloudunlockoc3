<?php
session_start();
include_once 'common.php';
include_once 'utils.php';

?>
<!DOCTYPE html>
<html lang="en">
    <head>
<?php
require_once 'header.php';
?>
    <link href="css/fileinput.min.css" media="all" rel="stylesheet" />        
    <script src="js/jquery-latest.min.js"></script>
    <script src="js/bootstrap.min.js"></script>    
    <script src="js/fileinput.min.js"></script>
    <style>
        .grayscale {
          filter: gray; /* IE6-9 */
          -webkit-filter: grayscale(1); /* Google Chrome, Safari 6+ & Opera 15+ */
        }        
    </style>    
    <title><?php echo APP_NAME; ?>, Unlock</title>
</head>
<body>

<!-- Navigation -->
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?= 'index.php' ?>"><?php echo APP_NAME; ?></a>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li>
                    <a href="index.php">Home</a>
                </li>
                <li>
                    <a href="contact.php">Contact</a>
                </li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li>
                    <a href="#">
                        <span class="badge"><?php
if (file_exists(LOG_FILE)){
    $links = simplexml_load_file(LOG_FILE);
    $count = 0;
    if ($links){
        $count = count($links);
    }
    echo $count;
} else {
    echo '0';
}
?></span> Files unlocked
                    </a>
                </li>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container -->
</nav>

<div class="container">
    <div class="page-header">

        <h1>Password Recovery Tool</h1>
        <p class="lead">Office documents with a password are <strong>encrypted</strong> files. There is no way to decrypt those files without <a href="https://en.wikipedia.org/wiki/Brute-force_attack">brute force</a></p>
        <p>So you need to use a <strong>password recovery tool</strong> to perform a brute force, and this might requires some technical skills and takes a several hours using other <a href="#alternatives" onclick="showTab('alternatives')">alternative solutions</a>.</p>
        <p>The good news however, you can use the following script file which you can download and use it to decrypt your document using the most powerful brute force engine: <a href="#what-is-hashcat" onclick="showTab('hashcat')">hashcat</a>.</p>
        <p>Once you've download the archive file, extract its content to any directory, you also need to download and extract <a href="#what-is-hashcat" onclick="showTab('hashcat')">hashcat</a> to the same directory then click <strong>Unlock</strong> script file.</p>

    </div><!-- .page-header -->
    
    <div class="row">
        <div class="col-sm-12">
            <ul class="list-unstyled">
                <li><h4>1 - Upload your document (doc, docx, xls, xlsx, xlsm, ppt, pptx, pdf):</h4></li>
            </ul>
            <form enctype="multipart/form-data">
                <div class="form-group">
                    <input id="file_upload" name="file_upload[]" type="file" multiple class="file-loading" data-max-file-count="10" />
                    <!--<input id="file_upload" name="file_upload[]" type="file" class="file-loading" />-->
                </div>
            </form>         
        </div>

        <div class="col-sm-12 text-center">
            <p>File size limit: <strong><?= MAX_FILE_SIZE ?> Mb</strong></p>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <ul class="list-unstyled">
                <li><h4>2 - Download Password Recovery Tool (hashcat):</h4></li>
            </ul>
            <p>Here is the lastest version which you can download from the official website:
                <a href="https://hashcat.net/files/hashcat-3.6.0.7z"><strong>hashcat.7z (2.5 Mb)</strong></a>.</p>
            <p>If it doesn't work get it from this link:
            <a href="./hashcat.zip"><strong>hashcat.zip (4.34 Mb)</strong></a>            </p>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6">
            <ul class="list-unstyled">
                <li><h4>3 - Unlock your document:</h4></li>
            </ul>
            <p>Extract <a href="https://hashcat.net">hashcat</a> into a directory.</p>
            <p>Copy the script you've downloaded to the parent directory.</p>
            <p>Excute the download script and wait for brute force.</p>
        </div>
    </div>
    
    <hr />
    
    <div class="row">
        <div class="col-sm-12">
            <!-- NavTabs !-->
            <ul class="nav nav-tabs" role="tablist" id="tablist">
                <li role="presentation" class="active"><a href="#options" aria-controls="options" role="tab" data-toggle="tab">Options</a></li>
                <li role="presentation"><a href="#hashcat" aria-controls="hashcat" role="tab" data-toggle="tab">Hashcat</a></li>
                <li role="presentation"><a href="#alternatives" aria-controls="alternatives" role="tab" data-toggle="tab">Alternative Solutions</a></li>
            </ul>
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="options">
                    <div class="row">
                        <div class="col-sm-12">
                        <br />
                        <form id="form-unlock" action="#" method="post">
                            <fieldset>
                            <legend>Brute force Options:</legend>
                            <div class="form-group">
                                <!--label class="control-label">Method:</label>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <label class="radio-inline"><input type="radio" id="method-wordlist" name="unlock-method" value="1" />WordList</label>
                                        <label class="radio-inline"><input type="radio" id="method-dictionary" name="unlock-method" value="2" />Dictionary</label>
                                        <label class="radio-inline"><input type="radio" id="method-bruteforce" name="unlock-method" value="3" checked="checked" />Brute-force</label>
                                    </div>
                                </div !-->
                            </div>
                            <div class="form-group">
                                <input id="increment-mode" name="increment-mode" type="checkbox" checked="checked"/>
                                <label for="increment-mode" data-toggle="tooltip" title="Specifies that the length of the password candidates shouldn't be fixed, but increase in length" >Increment Mode</label>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-3">
                                        <label class="control-label" for="hash_min">Min: </label><input class="form-control" id="hash_min" name="hash_min" value="1" data-toggle="tooltip" title="The minimum length for the increment mode" />
                                    </div>
                                    <div id="div-max" class="col-sm-3">
                                    <label class="control-label" for="hash_max">Max: </label><input class="form-control" id="hash_max" name="hash_max" value="10" data-toggle="tooltip" title="The maximum length for the increment mode" />
                                    </div>
                                </div>
                            </div>
                            <div id="div-mask" class="form-group">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <label class="control-label" for="mask" data-toggle="tooltip" title="Mask is combination of Charset">Mask: </label>
                                        <input class="form-control" type="text" id="mask" name="mask" value="?l?l?l?l?l?l?l?l?l?l" data-toggle="popover" data-content="Possible charsets:<pre>l: abcdefghijklmnopqrstuvwxyz<br />u: ABCDEFGHIJKLMNOPQRSTUVWXYZ<br />d: 0123456789<br />h: 0123456789abcdef<br />H: 0123456789ABCDEF<br />s: (32 symbols)<br />a: ?l?u?d?s<br /></pre>" title="Specifies the charsets" data-placement="right" data-html="true" />
                                        <span id="msg-mask-error" class="text-danger hidden">The length of the Mask should be not longer than Max value.</span>
                                    </div>                           
                                </div>
                            </div>
                            <hr />

                            <div class="form-group">
                                <input id="show-status" name="show-status" type="checkbox" />
                                <label for="show-status" data-toggle="tooltip" title="Check this option so hashcat showes the evolution of brute force" >Show Status</label>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-3">
                                        <label class="control-label" for="status-timer">Status Timer: </label>
                                        <input class="form-control" id="status-timer" name="status-timer" value="10" disabled="true" />
                                    </div>
                                </div>
                            </div>
                            </fieldset>
                            <br />
                            <fieldset>
                                <legend>GPU Options</legend>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <label class="control-label" for="workload" data-toggle="tooltip" title="In order to give the GPU more breathing room to handle the desktop you can set a lower workload profile">Workload Profile:</label>
                                            <select class="form-control" id="workload" name="workload" data-toggle="popover" data-html="true" data-content="Enable a specific workload profile:<pre>Reduced: performance profile (low latency desktop)<br />Default: performance profile<br />Tuned: performance profile (high latency desktop)<br /></pre>" title="Workload Profile">
                                                <option value="1">Reduced</option>
                                                <option value="2" selected="selected">Default</option>
                                                <option value="3">Tuned</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <label class="control-label" for="gpu-temp">Abort at °C: </label>
                                            <input class="form-control" type="text" id="gpu-temp" name="gpu-temp" value="80" data-toggle="tooltip" title="For security reason hashcat will abort at specific GPU temperature." />
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                            <div class="form-group">
                                <input type="checkbox" id="cpu-only" name="cpu-only" data-toggle="tooltip" title="Check this option if your GPU is not supported, so hashcat will use your cpu instead." />
                                <label class="control-label" for="cpu-only">Use CPU Only</label>
                            </div>
                            <hr />
                            <!-- div class="form-group">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <input type="checkbox" id="download-hashcat" name="download-hashcat" value="false" />
                                        <label class="control-label" for="download-hashcat">Download hashcat (> 4 Mb)</label>
                                    </div>
                                </div>
                            </div !-->
                            <br />
                            <fieldset>
                                <legend>Script Options:</legend>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <input type="checkbox" id="check_mimetype" name="check_mimetype" value="true" />
                                        <label class="control-label" for="check_mimetype" data-toggle="tooltip" title="If your file is rejected because of the its type, you can bypass file check mimetype.">Check File MimeType</label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <label class="control-label" for="osystem">Operation System:</label>
                                        <select class="form-control" id="osystem" name="osystem" data-toggle="tooltip" title="If you select Windows the script will be .bat file otherwise .bin file will be generated.">
                                            <option value="exe" selected="selected">Windows</option>
                                            <option value="bin">Linux</option>
                                        </select>
                                    </div>
                                </div>
                                </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <label class="control-label" for="os_arc" data-toggle="tooltip" title="For 64 bit machine you need to use hashcat64 file. set 32 bit to use hashcat32 instead.">OS Architecture Type: </label>
                                        <label class="radio-inline"><input type="radio" id="os_arc_64" name="os_arc" value="64" checked="checked" />64 bit</label>
                                        <label class="radio-inline"><input type="radio" id="os_arc_32" name="os_arc" value="32" />32 bit</label>
                                    </div>
                                </div>
                            </div>
                            </fieldset>
                        </form>
                        </div>
                    </div>
                </div><!-- #options -->
                <div role="tabpanel" class="tab-pane" id="hashcat">
                    <div class="row">
                        <div class="col-sm-12">
                            <h3><a href="#" id="what-is-hashcat">Hashcat - Advanced Password Recovery Tool</a></h3>
                            <p><a href="https://hashcat.net">Hashcat</a> is an advanced password recovery tool, you must be able to use the <strong>command line</strong> to use this software.</p>
                            <p>You also need to read the doc <a href="https://hashcat.net/wiki/" target="_blank">hashcat Documentation.</a> and you might also need to check the <a href="https://hashcat.net/faq" target="_blank">FAQ</a> as well.</p>
                            <p>If you want to set hashcat options, you can use the form options before uploading documents.</p>
                        </div>
                        <div class="col-sm-12">
                            <h3>How it works?</h3>
                            <p>After executing the script file, you need to wait for brute force operation, which might take several hours, in fact it depends on the length and complexity of the password used to encrypt the document.</p>
                            
                            <img src="img/unlocking.png" />
                            <p>Once the password found, you will see the password in the screen console if you're running the script from command line, like this:</p>
                            <img src="img/unlocked.png" />
                            <p>If you can't see the password, check the file <strong>hashcat.potfile</strong> (in the hashcat directory) which stores every password founded, and thus won't be cracked again.</p>
                            <p>Otherwise (if you the password couldn't be found after a long time waiting) try different options (increase length, increase complexity...etc.)</p>
                        </div>
                    </div>
                </div><!-- #hashcat -->
                <div role="tabpanel" class="tab-pane" id="alternatives">
                    <h3><a href="#" id="alternative-solutions">Alterntive Solutions</a></h3>
                    <p>Here are some alternative -none free- solutions which you can try at your own:</p>
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Price</th>
                                <th>Free Trial Limitation</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>1</td>
                                <td><a href="http://www.esofttools.com/excel-password-recovery.html" target="_blank">eSoftTools Excel Password Recovery</a></td>
                                <td><strong>$19</strong> (Personal License), $29 (Academic License), $49 (Enterprise License), $149 (Commercial License)</td>
                                <td>All office passwords are recovered, but you can only view and use 3 or less characters of password.</td>
                            </tr>
                            <tr>
                                <td>2</td>
                                <td><a href="http://www.perfectdatasolutions.com/excel-password-recovery-software.html" target="_blank">Perfect Data Solutions - Excel Password Recovery Software</a></td>
                                <td><strong>$19</strong> (Personal License), $49 (Enterprise License), $79 (Business License)</td>
                                <td>You can also recover first three character of the password at free of cost but to recover complete excel password you have to download full version.</td>
                            </tr>
                            <tr>
                                <td>3</td>
                                <td><a href="https://www.isunshare.com/office-password-genius.html" target="_blank">iSunshare Office Password Genius</a></td>
                                <td><strong>$29.95</strong> (Standard), $49.95 (Professional), $69.95 (Advanced)</td>
                                <td>3 and less characters of Office password can be recovered by free trial version</td>
                            </tr>
                            <tr>
                                <td>4</td>
                                <td><a href="http://www.daossoft.com/products/office-password-rescuer.html" target="_blank">Daossoft Office Password Rescuer</a></td>
                                <td><strong>$24.95</strong></td>
                                <td>All office passwords are recovered, but you can only view and use 3 or less characters of password.</td>
                            </tr>
                            <tr>
                                <td>5</td>
                                <td><a href="https://www.passware.com/kit-basic/" target="_blank">Passware Kit Basic</a></td>
                                <td><strong>$49</strong> (Basic), $79 (Standard), $195 (Plus), $795 (Business), $995 (Forensic)</td>
                                <td>Recovers either the first 3 letters of passwords, allows each of the attacks to work for up to 1 minute,resets a 'Demo12345' password only</td>
                            </tr>
                            <tr>
                                <td>6</td>
                                <td><a href="http://www.passwordlastic.com/office-password-recovery-lastic" target="_blank">Office Password Recovery Lastic</a></td>
                                <td><strong>$59.95</strong> (Personal), $119.85 (Business)</td>
                                <td>Partial decryption of MS Word 97/2000/XP/2003 documents protected with a password to open. Recovering of modify passwords up to three characters long for MS Word 97/2000/XP/2003 documents...(and more...)</td>
                            </tr>
                        </tbody>
                    </table>
                </div><!-- .tab-pane !-->
            </div><!-- tab-content !-->
        </div>
    </div>


    <hr />
    <div class="row">
        <div class="col-sm-12">
            <div class="alert alert-info">
                <h4>DISCLAIMER:</h4>
                <p>If the original file does not belong to you and there was a password, then probably there is a reason for blocking access to editing and/or copying contents of this document.</p>
                <p>By downloading this file, you assume all the responsibilities for an action that could infringe laws and could be legally prosecuted.</p>
                <p>We accept no liability for the content of this file, or for the consequences of any actions taken on the basis of the service provided.</p>
            </div>
        </div>
    </div>
    
    <div class="row">
        <div class="col-sm-12">

        </div>
       <!-- Footer -->
        <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p>Copyright &copy; <?php echo APP_NAME.' '.date('Y'); ?></p>
                </div><!-- /.col-lg-12 -->
            </div><!-- /.row -->
        </footer>
       
    <script>
    $(function(){
        $('[data-toggle="tooltip"]').tooltip();
        $('[data-toggle="popover"]').popover();

        $("#file_upload").fileinput({
            uploadAsync: true,
            uploadUrl: 'generate.php',
            allowedPreviewTypes: [],
            fileActionSettings: {
                showZoom: false
            },
            allowedFileExtensions: ['xls', 'xlsx', 'xlsm', 'doc', 'docx', 'ppt', 'pptx', 'pdf'],
            uploadExtraData: function(){
                return {
                    check_mimetype: $('#check_mimetype').is(':checked'),
                    hash_min: $('#hash_min').val(),
                    hash_max: $('#hash_max').val(),
                    inc_mode: $('#increment-mode').is(':checked'),
                    mask: $('#mask').val(),
                    show_status: $('#show-status').is(':checked'),
                    status_timer: $('#status-timer').val(),
                    /*workload: $('#workload').val(),*/
                    cpu_only: $('#cpu-only').is(':checked'),
                    gpu_temp: $('#gpu-temp').val(),
                    osystem: $('#osystem').val(),
                    os_arc: $('input[name=os_arc]:checked').val()
                    
                };
            }/*,
            initialPreview: [
                "<img src='img/excel_locked.png' class='file-preview-image' alt='Excel' title='Excel'>"
            ],
            overwriteInitial: false,
            maxFileSize: 1000,
            maxFilesNum: 10,
            maxFileCount: 10,
            slugCallback: function (filename) {
                return filename.replace('(', '_').replace(']', '_');
            }*/
        }).on('fileuploaded', function(event, data, previewId, index){
//            console.log(data.filenames[0]);
//            console.log(data.formData);
//            console.log('event: ' + event);
//            console.log('previewId: ' + previewId);
//            console.log('index: ' + index);
        });
        
//        $('#file_upload').on('fileuploaded', );        
        
        var hash_min = $('#hash_min');
        var hash_max = $('#hash_max');
        var inc_mode = $('#increment-mode');
        var mask = $('#mask');
        
        $(inc_mode).change(function(){
            var value = !$(this).is(':checked');
            $(hash_min).attr('disabled', value);
            $(hash_max).attr('disabled', value);
        });
        var status_timer = $('#status-timer');
        $('#show-status').change(function(){
           $(status_timer).attr('disabled', !$(this).is(':checked'));
        });
        $('input[name=unlock-method]').change(function(){
            var value = $(this).val() != 3;
            $(mask).attr('disabled', value);
            $(inc_mode).attr('disabled', value);
            $(hash_min).attr('disabled', value);
            $(hash_max).attr('disabled', value);
        });
                
        var checkMask = function(){
            
            var max = $(hash_max).val().trim();
            var msk = $(mask).val().trim();
            if ((!isNaN(max)) && msk !== '' ){
                if ( (max*2) <  msk.length ){
                    toggleErrorMask(true);
                } else {
                    toggleErrorMask(false);
                }
            }
        }
        
        function toggleErrorMask(value){
            if (value){
                $('#div-max').addClass('has-error');
                $('#div-mask').addClass('has-error');
                $('#msg-mask-error').removeClass('hidden');
            } else {
                $('#div-max').removeClass('has-error');
                $('#div-mask').removeClass('has-error');
                $('#msg-mask-error').addClass('hidden');
            }
        }
        
        $(mask).change(checkMask);
        $(hash_max).change(checkMask);
        
    }); //$({})
    
    function showTab(tabName){
        $('#tablist a[href="#'+tabName+'"]').tab('show');
    }
   
    </script>   
    
</div><!-- .container -->

</body>

</html>