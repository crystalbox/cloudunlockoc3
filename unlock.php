<?php
session_start();
include_once 'common.php';
include_once 'utils.php';

?>
<!DOCTYPE html>
<html lang="en">
    <head>
<?php
require_once 'header.php';
?>
        <script src="js/jquery-latest.min.js"></script>
        <title><?php echo APP_NAME; ?>, Unlock</title>
    </head>
    <body>

<!-- Navigation -->
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?= 'index.php' ?>"><?php echo APP_NAME; ?></a>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li>
                    <a href="index.php">Home</a>
                </li>
                <li>
                    <a href="contact.php">Contact</a>
                </li>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container -->

</nav>

<div class="container">
	<div class="row">
            <div class="col-sm-12">

<?php
getPost($unlock_workbook, 'unlock-workbook');
getPost($unlock_sheets, 'unlock-sheets');
$is_excel = isPost('btnexcel');
$is_word = isPost('btnword');
$is_slides = isPost('btnslides');

//check for hidden input field
if (!isPost('hfile')){
    redir("index.php");
} else {
    $file_id = NULL;
    if ($is_excel){
        $file_id = 'xlsx';
    } else if ($is_word){
        $file_id = 'docx';
    } else if ($is_slides){
        $file_id = 'pptx';
    }
    
    if ($file_id == NULL){
        redir("index.php");
        return;
    }
	$file_dir = UPLOAD_DIR;
        //delete all files (xlsx & docx & pptx && zip) before import
        delFiles($file_dir, "*.*x");
        delFiles($file_dir, "*.zip");
	if((!empty($_FILES[$file_id])) && ($_FILES[$file_id]['error'] == 0)) {
	//  foreach ($_FILES as $file) {
	//    $file = $_FILES[$file_id];
		//Check if the file is valid and it's size is not exceeded
		$filename = $_FILES[$file_id]['name'];
		$tmp_name = $_FILES[$file_id]['tmp_name'];
                $filesize = $_FILES[$file_id]['size'];
		$ext = strtolower(pathinfo(basename($filename), PATHINFO_EXTENSION));
                
		$finfo = new finfo();
		if (!empty($tmp_name)){
		  $fileMimeType = $finfo->file($tmp_name, FILEINFO_MIME_TYPE);
		} else {
		  $fileMimeType = "";
		  /*
		  // those for Chrome
		  unknown: 				application/octet-stream
		  zip: 					application/zip
		  pdf: 					application/pdf
		  doc: 					application/msword
		  docx: 				application/vnd.openxmlformats-officedocument.wordprocessingml.document
		  docx (zip):                           application/zip
		  docx (locked):                        application/octet-stream
		  xls: 					application/vnd.ms-office
		  xls(2003):                            application/vnd.ms-excel
		  xlsx (protected):                     application/CDFV2-corrupt
		  xlsx (unprotected):                   application/vnd.openxmlformats-officedocument.spreadsheetml.sheet
		  xlsx/xlsm (locked):                   application/vnd.openxmlformats-officedocument.spreadsheetml.sheet
                  xlsxm (in other host)                 application/vnd.ms-excel.sheet.macroEnabled.12
		  vcf: 					text/x-vcard
		  pptx (locked): 			application/vnd.openxmlformats-officedocument.presentationml.presentation
		  pptx (protected): 			application/CDFV2-corrupt
		  mp4: 					video/mp4
		  accdb: 				application/x-msaccess
		  exe: 					application/x-dosexec
		  dll: 					application/x-dosexec
		  jpeg: 				image/jpeg
		  html: 				text/html
		  txt: 					text/plain
		  */
		}
//		die($fileMimeType);
                $encrypted_mimetype = array('application/CDFV2-encrypted', 'application/CDFV2-corrupt');
                if (in_array($fileMimeType, $encrypted_mimetype)){
                    $hash = trim(get_hash_file($tmp_name));
                    if (!empty($hash)){
                        redirSession('decrypt_file.php', 'file_hash', $hash);
                    } else {
                        echo '<div class="alert alert-info text-center">'.
                        '<p>This file is <strong>encrypted</strong> with a password, so it cannot be unlocked.</p>'.
                        '<p>Please consider to try our <a href="password_recovery.php">Password Recovery Tool</a></p></div>';                        
                    }
                } else if ($is_excel){
                    if (($ext != "xlsx" && $ext != "xlsm" ) || 
                            ($fileMimeType != 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' &&
                            $fileMimeType != 'application/vnd.ms-excel.sheet.macroEnabled.12')) {
                        echo '<p>Sorry, only Excel (XLSX/XLSM) can be unlocked!</p>';
                        echo ('<p>MimeType Detected: <b>'.$fileMimeType.'</b></p>');
                        return;
                    } 
                } else if($is_word) {
                    if (($ext != "docx") || ($fileMimeType != 'application/octet-stream')) {
                        echo '<p>Sorry, only Word Document (DOCX) can be unlocked!</p>';
                        echo ('<p>MimeType Detected: <b>'.$fileMimeType.'</b></p>');
                        return;
                    }
                } else if($is_slides) {
                    if (($ext != "pptx") || ($fileMimeType != 'application/vnd.openxmlformats-officedocument.presentationml.presentation')) {
                        echo '<p>Sorry, only PowerPoint Document (PPTX) can be unlocked!</p>';
                        echo ('<p>MimeType Detected: <b>'.$fileMimeType.'</b></p>');
                        return;
                    }
                } else {
                    echo '<p>Sorry, this file is not supported.</p>';
                    echo ('<p>MimeType Detected: <b>'.$fileMimeType.'</b></p>');
                    return;
                }
		
                if ($filesize > (MAX_FILE_SIZE * 1024*1024 /* = in Mb */) ) {
                    echo '<div class="alert alert-danger text-center"><p>Sorry, maximum file size limit has been exceeded!</p></div>';
                    return;
		}
                
    //Determine the path to which we want to save this file
      $newname = $file_dir.time().'_'.$filename;
      //Check if the file with the same name is already exists on the server
      if (!file_exists($newname)) {
        //Attempt to move the uploaded file to it's new place
        if ((move_uploaded_file($tmp_name, $newname))) {
            unlock_file($newname, $unlock_workbook, $unlock_sheets, $ext);
//           echo "It's done! The file has been saved as: ".$newname;
        } else {
           echo '<div class="alert alert-danger text-center"><p>Nothing unlocked</p>';
           echo "<p>Error: A problem occurred during file upload!</p></div>";
           return;
        }
      }
	} else {
            echo '<div class="alert alert-danger text-center"><p>No file uploaded!</p></div>';
            echo '<p>Back to <a href="index.php">Home</a></p></div>';
            return;
	}
}

/*
	this way should work on XLSX & XLSXM file
	
	For Workbook(whole excel file) Settings:
	xl>workbook.xml

	For Individual Worksheet File Settings:
	xl>worksheets>sheet1.xml [sheet2.xml] – etc…
	
	for more info:
	http://www.minnesotaithub.com/2014/03/crack-microsoft-excel-password-free-excel-2007-2010-2013/
  
*/
function unlock_file($file_name, $unlock_workbook, $unlock_sheets, $file_type){
//    die(realpath($file_name));
    $path = realpath(dirname($file_name)).DIRECTORY_SEPARATOR.pathinfo($file_name)['filename'];
    if (!file_exists($path)) {
        mkdir($path, 0777, true);
    }
    //unzip the new file
    if (unzip($file_name, $path)){
//            echo "<p>file <b>$file_name</b> extracted to <b>$path</b></p>";
            echo '<p class="text-success">File extracted successfully... OK</p>';
    } else {
            echo "error. Cannot extract <b>$file_name</b>";
            return;
    }
    
    if ($file_type == 'xlsx' || $file_type == 'xlsm'){
        if ($unlock_workbook){
            unlock_workbook($path);
        }
        if ($unlock_sheets){
            unlock_sheets($path);
        }
    } else if ($file_type == 'docx') {
        unlock_word($path);
        
    } else if ($file_type == 'pptx'){
        unlock_slides($path);
    }
    $down_file = $path.'_unlocked.'.$file_type;
    
    zipAll($path, $down_file);
    //clean up
    if (file_exists($path)) {
        delTree($path);
    }
    if (file_exists($file_name)){
        unlink(realpath($file_name));
    }
//    echo "File path: $down_file <br />";
    if (zip_file($down_file)){
        $link_zip = UPLOAD_DIR.basename(replace_extension($down_file, 'zip'));// pathinfo($file_name)['filename'].'.zip';
        if (file_exists($link_zip)){
            unlink($down_file);
            echo '<p class="text-success">File Unlocked, generating download link... OK</p>';
            $size = filesize_formatted($link_zip);
            if (DEBUG_MODE){
                $down_file = $link_zip;
            } else {
                $h = shortener(getUrl().DIRECTORY_SEPARATOR.$link_zip);
                $down_file = $h !== ''? ('http://adf.ly/'.$h) : $link_zip;
            }
            echo '<div class="alert alert-success center"><p>Download link: </p><a target="_blank" href="'.
                    $down_file.'">Download file (size: '.$size.', type: zip, ads: Adfly)</a></div>';
            
            $data = array(
                    'file' => $file_type,
                    'url' => $down_file,
                    'date' => date('d-m-Y'),
                    'country'=> ip_info("Visitor", "Country"),
                    'state' => ip_info("Visitor", "State")
                    //'city' => ip_info("Visitor", "City")
                    );
            log_to_file($data);
            
            /*
            // its also possible de output zip file
            header('Content-type: application/zip');
            readfile('test.zip');
            */
        }// else echo "Archive $link_zip not found.";
    }// else {
        //echo "File $down_file not found.";
    //}
}

function unlock_workbook($folder_path){
    $workbook_xml = $folder_path.DIRECTORY_SEPARATOR.'xl'.DIRECTORY_SEPARATOR.'workbook.xml';
    if (file_exists($workbook_xml)){
        //1.for WorkBook find <workbookProtection> in "/xl/workbook.xml"
        $content = file_get_contents($workbook_xml);
        $xml = simplexml_load_string($content);// or die("<p>Error: Cannot create object</p>");
    //then update attribute lockStructure="0"
        if ($xml->workbookProtection){
            $xml->workbookProtection['lockStructure'] = 0;
        } else {
            echo ('<p class="text-success">The Workbook does not seem to be locked</p>');
            return;
        }
        //in case we want to remove a whole tag
    //        $dom=dom_import_simplexml($seg);
    //        $dom->parentNode->removeChild($dom);            
        $xml->asXML($workbook_xml);
    }
}

function unlock_sheets($folder_path){
    //2. for WorkSheet find <sheetProtection>
    //then delete the whole tag and save
    $rootPath = $folder_path.DIRECTORY_SEPARATOR.'xl'.DIRECTORY_SEPARATOR.'worksheets';//.DIRECTORY_SEPARATOR.'sheet1.xml';
    $files_sheets = new RecursiveIteratorIterator(
        new RecursiveDirectoryIterator($rootPath),
        RecursiveIteratorIterator::LEAVES_ONLY
    );
    $i = 0;
    foreach ($files_sheets as $sheet => $file_sheet){
        if (!$file_sheet->isDir()){
//            $sheet = $file_sheet->getRealPath();
            if (!file_exists($sheet)){
                die("<div class='error'>file <b>".$sheet."</b> not found.</div>");
            }
            $sheet_content = file_get_contents($sheet);
            $sheet_xml = simplexml_load_string($sheet_content);
        //        foreach ($sheet_xml->sheetProtection as $sp){
                if ($sheet_xml->sheetProtection){
                    $dom = dom_import_simplexml($sheet_xml->sheetProtection);
                    $dom->parentNode->removeChild($dom);
                } else {
                    echo ('<p class="text-success">Worksheet "N°'.(++$i).'" does not seem to be locked</p>');
                }
        //        }
            $sheet_xml->asXML($sheet);
        }
    }    
}

function unlock_word($folder_path){
    //for docx, go to /word/settings.xml and remove tag <w:documentProtection />, where xml namespace:
    //xmlns:w="http://schemas.openxmlformats.org/wordprocessingml/2006/main"
    $settings_xml = $folder_path.DIRECTORY_SEPARATOR.'word'.DIRECTORY_SEPARATOR.'settings.xml';
    if (file_exists($settings_xml)){
        //1.for WorkBook find <workbookProtection> in "/xl/workbook.xml"
        $content = file_get_contents($settings_xml);
        $xml = simplexml_load_string($content);// or die("<p>Error: Cannot create object</p>");
        $namespaces = $xml->getDocNamespaces();
        if (count($namespaces['w']) == 0){
            echo ('<div class="alert alert-warn text-center"><p>Sorry, this Document cannot be unlocked!</p>');
            return;
        }
        $xml->registerXPathNamespace("w", $namespaces['w']);
        $tag = $xml->xpath('/w:settings/w:documentProtection');
        if (count($tag)>0){
            $dom = dom_import_simplexml($tag[0]);
            $dom->parentNode->removeChild($dom);
            $xml->asXML($settings_xml);
        } else {
            echo ('<div class="alert alert-warn text-center"><p>This Document does not seem to be locked</p>');
            return;
        }
    }
}

function unlock_slides($folder_path){
/*      for PowerPoint file pttx, here is what you need to do:
        unzip the file then remove from file /ppt/presentation.xml the whole tag <p:modifyVerifier />
        then save & overwrite, this can found here:
        http://www.isocial.it/fr/rimuovere-la-password-powerpoint-pptx-bloccati-sola-lettura-guida/#
*/
    $presentation_xml = $folder_path.DIRECTORY_SEPARATOR.'ppt'.DIRECTORY_SEPARATOR.'presentation.xml';
    if (file_exists($presentation_xml)){
        $content = file_get_contents($presentation_xml);
        $xml = simplexml_load_string($content);// or die("<p>Error: Cannot create object</p>");
        $namespaces = $xml->getDocNamespaces();
        if (count($namespaces['p']) == 0){
            echo ('<div class="alert alert-warn text-center"><p>Sorry, this Document cannot be unlocked!</p>');
            return;
        }
        $xml->registerXPathNamespace("p", $namespaces['p']);
        $tag = $xml->xpath('/p:presentation/p:modifyVerifier');
        if (count($tag)>0){
            $dom = dom_import_simplexml($tag[0]);
            $dom->parentNode->removeChild($dom);
            $xml->asXML($presentation_xml);
        } else {
            echo ('<div class="alert alert-warn text-center"><p>This Document does not seem to be locked</p>');
            return;
        }
    }
}
  
/*
function delTree($directory) {
    foreach(glob("{$directory}/*") as $file)
    {
        if(is_dir($file)) { 
            delTree($file);
        } else {
            unlink($file);
        }
    }
    rmdir($directory);
}*/
  
/*
if ( 
	(isPost('btn_contact')) &&
	(isPost('username')) &&
	(isPost('email')) &&
	(isPost('subject'))
	){
		$nom = getPostValue('username');
		$email = getPostValue('email');
		$message = getPostValue('subject');

if (mail('bitsnaps@yahoo.fr', 'BitSnip, from: '.$nom, $message, 
	'From: '.$nom.' <'.$email.'>'."\n".'MIME-Version: 1.0'."\n".'Content-type: text/html; utf-8'."\r\n")) {
		echo '<div style="color:green;"><p>Your message has been sent, we will replay as soon as possible.<br />Thanks.</p></div>';
	} else {
		echo '<div style="color:green;"><p>Your message could not be send, </p><p>Please try again later.</p></div>';
	}

} else {
	echo '<div style="color:red;"><p>An error has occured. </p><p>Please try again later.</p></div>';
}
*/
?>
		<div class="row">
                    <div class="col-sm-12">
                            <p>Back to <a href="<?php echo basename(__FILE__); ?>/..">Home</a></p>
                    </div>
		</div><!-- .row -->

		</div><!-- .col-sm-12 -->
                
                <div class="col-sm-12">
                    <div class="alert alert-info">
                    <p><strong>DISCLAIMER: </strong></p>
                    <p>if the original file does not belong to you and there was a password, then probably there is a reason for blocking access to editing and/or copying contents of this document.</p>
                    <p>By downloading this file, you assume all the responsibilities for an action that could infringe laws and could be legally prosecuted.</p>
                    <p>We accept no liability for the content of this file, or for the consequences of any actions taken on the basis of the service provided.</p>
                    </div>
                </div><!-- .col-sm-12 -->
	</div><!-- .row -->
        
       <!-- Footer -->
        <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p>Copyright &copy; <?php echo APP_NAME.' '.date('Y'); ?></p>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </footer>
       
</div><!-- .container -->

</body>

</html>