<?php
define('BR', '<br />');
//check for valid email
//usage: if (isEmail('exemple@server.com'))
function isEmail($email){return filter_var( $email, FILTER_VALIDATE_EMAIL );}//>= php5.2.0
//usage: if (isValidEmail('exemple@server.com'))
function isValidEmail($email){return preg_match("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$", $email);}
//encodes an ISO-8859-1 string to UTF-8
//usage: echo '<p>'.setUtf8('caractères...').'</p>';
function setUtf8($text){ return utf8_encode($text);}
//convert to HTML codes
//usage: echo '<p>'.toHtml('<h2>Header 2</h2>').'</p>';
function toHtml($text){ return htmlentities($text, ENT_COMPAT, 'UTF-8');}//NOTE: html_entity_decode() is the opposite of htmlentities() in that it converts all HTML entities in the string to their applicable characters.
//convert to unicode_utf8 and addslashes
//usage: $res=getQuery('SELECT * FROM users WHERE userName='.toSql($user));
function toSql($param){ return utf8_decode(addslashes($param));}
//encode to UTF-8 & convert to HTML codes
//usage: echo '<p>'.fixText('caractères...').'</p>';
function fixText($text){ return toHtml(setUtf8($text));}
//wrap text in a tag
//usage: echo wrap('texte...', 'p');
function wrap($val, $tag, $param=''){return "<$tag$param>$val</$tag>";}
//same as wrap() with fixText()
function wrapf($val, $tag, $param=''){return "<$tag$param>".fixText($val)."</$tag>";}
function wraph($val, $tag, $param=''){return "<$tag$param>".toHtml($val)."</$tag>";}
//wrap content in <table> tag
//echo table( td(td('cell')), ' class="tableData"');
function table($val, $parm=''){return wrap($val, 'table', $parm);}
//wrap text in <td> tag
//echo '<table><tr>'.td('cell').'</tr></table>';
function td($val, $parm=''){return wrap($val, 'td', $parm);}
function tdf($val, $parm=''){return wrapf($val, 'td', $parm);}
function tdh($val, $parm=''){return wraph($val, 'td', $parm);}
//wrap text in <tr> tag
//echo '<table>'.td(td('cell'), ' class="className"').'</table>';
function tr($val, $parm=''){return wrap($val, 'tr', $parm);}
//wrap text in <a href=""> tag
//echo a('Click here', 'somtesite.com');
function a($text, $url='#', $parm=''){return wrap($text, 'a', ' href="'.$url.'"'.$parm);}
function af($text, $url='#', $parm=''){return wrapf($text, 'a', ' href="'.$url.'"'.$parm);}
function ah($text, $url='#', $parm=''){return a(toHtml($text), $url, $parm);}
//wrap text in <p> tag
//echo p('Page not found', ' class="error"');
function p($text, $parm=''){return wrap($text, 'p', $parm);}
function pf($text, $parm=''){return wrapf($text, 'p', $parm);}
function ph($text, $parm=''){return wrap(toHtml($text), 'p', $parm);}
//wrap text in <ul> tag
//echo ul('Unordred list', ' class="red");
function ul($text, $parm=''){return wrap($text, 'ul', $parm);}
//wrap text in <li> tag
//echo li('List Item', ' class="small");
function li($text, $parm=''){return wrap($text, 'li', $parm);}
//wrap text in <div> tag
//echo div(a('Click here', 'somtesite.com'));
function div($text, $parm=''){return wrap($text, 'div', $parm);}
//check if $_POST variable is set
function isPost($name){return (isset($_POST[$name]));}
//check if $_POST variable is set and not empty
function isSetPost($name){return (isPost($name) && ( ($p=trim($_POST[$name])) && !empty($p) )) ;}
//get $_POST variable, and return false if not set or empty
//usage: if (getPost(&$userName, 'username'))
function getPost(&$value, $name) {if (isSetPost($name)) {$value = trim($_POST[$name]);return TRUE;} else return FALSE;}
//save as getPost above returning a post value, usage: if (getPostValue('username') == 'a name')
function getPostValue($name) {return trim($_POST[$name]);}
//same as getPost() and allow null values, same usage
function getPostNull(&$value, $name) {if (isPost($name)) {$value = $_POST[$name];return TRUE;} else return FALSE;}
//secure version of getPost, same usage
function getPosts(&$value, $name) {if (isSetPost($name)) {$value = mysql_real_escape_string(stripcslashes(trim($_POST[$name])));return TRUE;} else return FALSE;}
//check if $_GET variable is set
function isGet($name){return (isset($_GET[$name]));}
//get $_GET variable, and return false if not set
//usage: if (get(&$key, 'searchkey'))
function get(&$value, $name){if (isGet($name)) {$value=$_GET[$name]; return TRUE;} else return FALSE;}//$value can be empty!
//check whether session is set or not
//usage: if (isSession('sessionName')){}
function isSession($name){return isset($_SESSION[$name]);}
//return session global variable
//usage: if (getSession(&$var, 'varName')){}
function getSession(&$value, $name){if (isSession($name)){$value=$_SESSION[$name];return TRUE;} else return FALSE;}
//usage: getSessionDie(&$var, 'varName')
function getSessionDie(&$value, $name){if (!getSession($value, $name)) die (wrapf('Désolé, votre page a expiré. Cliquez ici pour déconnecter', 'a', ' href="disconnect.php"'));}
//create session global variable
 //usage: setSessions('varName', $value)
function setSession($name, $value){$_SESSION[$name]=$value; return $value;}
//display query result returned by mysql_query
//usage: tableShow($res, tr(td('Field1').td('Field2')), ' class="header"'); 
function tableShow($table, $head='', $param=''){echo "<table$param>".wrap($head, 'thead').'<tbody>';while ($line=mysql_fetch_row($table)){echo '<tr>';foreach ($line as $data) echo tdf("$data"); echo '</tr>';}echo '</tbody></table>';}
//execute a query and return (Optionaly) its result
//uage: getQuery('SELECT * FROM users', true, tr(td('User Name').td('Password')));
function getQuery($req, $wantShow=FALSE, $head=''){/*if ($wantShow) die($req);*/ $res=mysql_query($req) or die("Exécution impossible: ".mysql_error().BR.$req); if ($wantShow) tableShow($res, $head); return $res;}
//execute a query and return its data as array
//uage: $dataArray=getTable('SELECT * FROM users');
function getTable($req, $wantShow=FALSE, $head=''){$res=getQuery($req, $wantShow, $head); return mysql_fetch_array($res);}
//redirect page
//usage: redir('page.php');
function redir($page, $replace = true){header("Location: $page", $replace);exit;}
//show a Disconnect link
function disconnect(){echo a('Déconnexion', 'disconnect.php');}
//check for empty query result
function notEmpty($res){return (mysql_num_rows($res)!=0);}
//reversed notEmpty()
function isEmpty($res){return !notEmpty($res);}
//lookup for value with id
//usage: $res=lookup('Users', 'userName', "'Mohamed'", true, 'userID, userName, password');
function lookup($table, $field, $value, $retTable=FALSE, $select='*'){$res=getQuery("SELECT $select FROM $table WHERE $field=$value");return $retTable?mysql_fetch_array($res):$res;}
//check for valid foreign key value
//usage: if (isValidId('Users', 'userName', "'Mohamed'"))
function isValidId($table, $field, $value){return notEmpty(lookup($table, $field, $value));}
//return $_SERVER['PHP_SELF']
function getSelf($parm=''){return $_SERVER['PHP_SELF'].$parm;}
//encrypt sha1 hash with salt "~i_*"
function cryptSha1($key){return crypt("~i_*".sha1($key));}
function isValidCryptSha1($key, $salt){return (crypt("~i_*".sha1($key), $salt)==$salt);}
//Unset session
function unSession($session){unset($_SESSION[$session]);}
//create a field (label+input)
//createField('User Name', 'username', 'text');
function createField($label, $name, $type, $class='', $val='', $readonly=false){
	$ro=$readonly?' readonly=\"readonly\"':'';$cl=$class?" class=\"$class\"":'';$v=$val?" value=\"$val\"":'';
	return td(wrap($label.'&#58;', 'label', " for=\"$name\"")).
	td("<input$cl type=\"$type\" id=\"$name\" name=\"$name\"$v$ro />");
}
//create a form without fields
//createForm('register.php', 'post', 'Register', createField('Your name', 'username', 'text'));
function createForm($action, $method, $legend, $fields, $id="", $class=""){
	$ID=$id?" id=\"$id\"":'';
	echo "<form$ID action=\"$action\" method=\"$method\"><fieldset>".wrap($legend, 'legend')."<table$class>".
	$fields.'</table></fieldset></form>';
}
function createInput($type, $id, $val='', $param=''){
	$v=$val?" value=\"$val\"":'';
	return "<input type=\"$type\" id=\"$id\" name=\"$id\"$v"."$param />";
}
//create a checkbox field (label after input)
//usage: createCheckbox('saveParam', 'Remember me?', ' class="cbx"')  
function createCheckbox($id, $label, $param=''){
	return createInput('checkbox', $id, '', $param).wrap($label, 'label', " for=\"$id\"");
}
//(deprecated)create a form with fields
//usage: createFields('Id', 'page.php', '', $arrayfields);
function createFields($legend, $page, $class='', $fields, $type="text"){
	echo "<form action=\"$page\" method=\"post\"><fieldset>".wrap($legend, 'legend')."<table$class>";
	foreach ($fields as $label => $name)
		echo tr(createField($label, $name, $type));
		/*echo tr(td(wrap($label.'&#58;', 'label', " for=\"$name\"")).
		td("<input type=\"text\" id=\"$name\" name=\"$name\" />"));*/
	echo tr(td('<input type="submit" value="Valider" />').
	td("<input type=\"hidden\" id=\"ok\" name=\"ok\" value=\"option\" />")).'</table></fieldset></form>';
}
function getOptionList($table, $selected=''){
	$ret='';
	while ($select=mysql_fetch_row($table))
		$ret.=wrapf("$select[0]", 'option', " value=\"".encrypt64($select[1])."\"".($selected==$select[1]?' selected="selected"':''));
	return $ret;
}
//create select tags (note $table should be array of 2d)
//usage: echo getSelections($res, 'sectionId');
function getSelections($table, $id, $class='', $selected=''){
	$ret="<select id=\"$id\" name=\"$id\"$class>";
	$ret.=getOptionList($table, $selected);
	/*while ($select=mysql_fetch_row($table))
		$ret.=wrapf("$select[0]", 'option', " value=\"".encrypt64($select[1])."\"");*/
return $ret.='</select>';
}
//create select/option (note $res should contains two column)
//usage: echo getOptions($res, 'Select:', 'sectionId');
function getOptions($table, $label, $id, $class='', $selected=''){
$ret=td(wrap($label.'&#58;', 'label', " for=\"$id\"")).'<td>'.getSelections($table, $id, $class?" class=\"$class\"":'', $selected).'</td>';
return (notEmpty($table)?$ret:false);
}
/*/create select/option (note $res should contains one column)
//getOptions($res)
function getOptions($table, $label, $id, $class=''){
$cl=$class?" class=\"$class\"":'';
$ret=td(wrap($label.'&#58;', 'label', " for=\"$id\""))."<td><select id=\"$id\" name=\"$id\"$cl>";
while ($select=mysql_fetch_row($table))
		$ret.=wrapf("$select[0]", 'option');
$ret.='</select></td>';
return (notEmpty($table)?tr($ret):false);
}*/
//redirection with session msg
//redirSession('index.php', 'login', 'success');
function redirSession($page, $var, $value=''){
	setSession($var, $value);
	redir($page);
}

function encrypt($str){
  $key = "abc123 as long as you want bla bla bla";
  for($i=0; $i<strlen($str); $i++) {
     $char = substr($str, $i, 1);
     $keychar = substr($key, ($i % strlen($key))-1, 1);
     $char = chr(ord($char)+ord($keychar));
     $result.=$char;
  }
  return base64_encode($result);
}


function decrypt($str){
  $str = base64_decode($str);
  $result = '';
  $key = "must be same key as in encrypt";
  for($i=0; $i<strlen($str); $i++) {
    $char = substr($str, $i, 1);
    $keychar = substr($key, ($i % strlen($key))-1, 1);
    $char = chr(ord($char)-ord($keychar));
    $result.=$char;
  }
  return $result;
}

function encrypt64($val){
  return strrev(base64_encode($val));	
}
function decrypt64(&$val){
  return $val=base64_decode(strrev($val));	
}

function saveToXML($fileName, $data){
    $fxml=fopen($fileName, 'w');
    fwrite($fxml, $data);
    fclose($fxml);	
}

function emptyXml($xmlElement){
    $arr = (array) $xmlElement;
    return (empty($arr));	
}

//today func for mysql
function today(){
    return date('Y-m-d H:i:s');
}
//returns current time
function now(){
    return date('H:i:s');
}
//format a given string using specific format
function toDate($d, $formatIn='d/m/Y', $formatOut='Y-m-d'){
    $out = DateTime::createFromFormat($formatIn, $d);
    return $out->format($formatOut);
}
//format a given string using specific format
function toTime($d, $formatIn='H:i:s', $formatOut='H:i'){
    $out = DateTime::createFromFormat($formatIn, $d);
    return $out->format($formatOut);
}

//retrieve column names form PDOStatment
function getColumnNames($query){
    $fields = array();
    for ($i=0; $i < $query->columnCount(); $i++){
            $col = $query->getColumnMeta($i);
            array_push($fields, $col);
    }
    return $fields;
}

//alternative to json_encode for PHP < 5.4 (fix utf8 encoding issue)
function raw_json_encode($input) {
    return preg_replace_callback(
        '/\\\\u([0-9a-zA-Z]{4})/',
        function ($matches) {
            return mb_convert_encoding(pack('H*',$matches[1]),'UTF-8','UTF-16');
        },
        json_encode($input)
    );
}

//get HTTP_REFERER page name
function getReferer(){
//    if (isset(filter_input('INPUT_SERVER', 'HTTP_REFERER'))){
	if (isset($_SERVER['HTTP_REFERER'])){
		$url = parse_url($_SERVER['HTTP_REFERER']);
//		$url = parse_url(filter_input(INPUT_SERVER, 'HTTP_REFERER'));
		return basename($url['path']);
	} else {
            return '';
        }
}

function getFullUrl(){
    return ((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off') 
        || $_SERVER['SERVER_PORT'] == 443?'https://':'http://')
        . $_SERVER['HTTP_HOST'] 
        . $_SERVER['REQUEST_URI'];
}

function filesize_formatted($path)
{
    $size = filesize($path);
    $units = array( 'B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB');
    $power = $size > 0 ? floor(log($size, 1024)) : 0;
    return number_format($size / pow(1024, $power), 2, '.', ',') . ' ' . $units[$power];
}


?>